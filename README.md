**INHALTSVERZEICHNIS**

[TOC]


# m322 - Benutzerschnittstellen entwerfen und implementieren

*Entwirft und implementiert Benutzerschnittstellen für eine Applikation. Beachtet dabei Standards und ergonomische Anforderungen.*

2. Lehrjahr, Q1/Q2, für Applikationsentwickler


[> **Modulidentifikation** ](https://www.modulbaukasten.ch/module/322/1/de-DE?title=Benutzerschnittstellen-entwerfen-und-implementieren)



<hr>

# Leistungsnachweise

- [**20%**] Zwei Kurzprüfungen (Checkpoints) 

- [**40%**] Projektdokumentation
	- Problem statement (Point of View, Scope)
	- Personas (Zielgruppe, Akteure / Benutzer)
	- User Journey mit Touchpoints
	- Assumption Map (festgehaltene Annahmen) 
	- Key Screens (Screen Flow)
	- Findings aus Benutzertests

- [**40%**] Klickbarer Prototyp


# Lektionenplan


## Grundsätzliche Idee

Die Lernenden erarbeiten anhand einer vorgegebenen Idee einen Prototyp für eine App. Das Vorhaben heisst z.B.: «Neue App für einen Zoo-Besuch». Durch einfache Benutzertests, mit mindestens fünf Benutzern, soll die Lösung jeweils validiert und anschliessend optimiert werden. Dabei sollen mindestens zwei Iterationen des Human-Centered-Design-Prozesses durchlaufen werden. 

Der Auftrag wird in einer Gruppenarbeit zu etwa 4 Personen durchgeführt. 


## Literaturvorschlag

Collaborative UX Design. Lean UX und Design Thinking: Teambasierte Entwicklung menschzentrierte Produkte (Steimle & Wallach, 2018. dpunkt.verlag)

[Literatur](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m322/literatur)


## Tools

**Figma** <https://www.figma.com>
- Figma Tutorial for UI Design - Course for Beginners <https://www.youtube.com/watch?v=jwCmIBJ8Jtc>
- Figma Tutorial 1 <https://youtu.be/3q3FV65ZrUs>
- Figma Tutorial 2 <https://youtu.be/FTFaQWZBqQ8>

**Adobe XD** <https://www.adobe.com/uk/products/xd>


## Videos
- [28 min, E, 2020, UX Design: How To Get Started (A Full Guide)](https://www.youtube.com/watch?v=t0aCoqXKFOU)
- [32 min, E, 2022, Figma Masterclass for Beginners (2022 Updated)](https://www.youtube.com/watch?v=II-6dDzc-80)
- [ 9 min, E, 4 Foundational UI Design Principles | C.R.A.P. ](https://www.youtube.com/watch?v=uwNClNmekGU)



## Modul-Ablauf, Tagesplan

| **Lektionen** | **Themen**       | **Kompetenzen** | **Tools** |
| ------------- | -----------------| --------------- | --------- |
<mark> == Tag 1  </mark>
| 1 <br/>(1)  | Überblick Modul und Einführung ins Thema:<br />Design Thinking / Human Centered Design / Notwendigkeit und Vorteile im Entwicklungsprozess (frühe Anpassungen = weniger Kosten, etc.) | A    | |
| 1 <br/>(2)  | Das Zusammenspiel von verschiedenen Berufen aufzeigen (Entwickler:in, UX-Research, Business Analysten, etc.) insbesondere die Unterscheidung zwischen Visual Design und UX-Design:<br />Klärung, dass dies unterschiedliche Disziplinen sind. | A    | |
| 1 <br/>(3)  | Auswahl (vorgegebener) Case: «Neue App für ein Zoo-Besuch»:<br />Neuartige App, die den Besuch im Zoo erleichtern und verbessern soll. z.B. für Familien, Einzelpersonen, Gruppen, Tickets kaufen, Touren sehen, Wo ist was, News, etc. | A    | |
| 2 <br/>(5)  | Analyse der User (Proto-)Personas und deren Aufgaben:<br />Welche Nutzergruppen haben wir und was ist deren primäre Aufgabe bzw. Bedürfnis? (z.B. Familien mit 2 Kindern an einem Samstag Nachmittag im Zoo) | A  | Theydo.io |
<mark> == Tag 2  </mark>
| 2 <br/>(7)  | Ableitung von Kunden/Nutzer, Probleme, Lösungsansätze, Metriken, Stakeholder, Randbedingungen, Risiken («Problem Statements») | A |    |
| 1 <br/>(8)  | Annahmen/Hypothesen auflisten und priorisieren: Von welchen Annahmen in Bezug auf die App bzw. die User gehen wir aus? Wie sicher sind wir uns dabei und was wäre die Auswirkung, wenn dem nicht so ist? Grundlage für späteres User Testing<br />z.B. User wollen Tickets digital kaufen | A    | |
<mark> == Tag 3  </mark>
| 1 <br/>(9)  | Ist-Zustand des heutigen Flows erstellen (= "Journey-Map"):<br />Wie funktioniert ein Besuch im Zoo heute? | A    | Miro |
| 2 <br/>(11) | Mögliche Chancen zur Optimierung identifizieren in der Journey-Map identifzieren anhand von bekannten Informationen (= «Opportunity Areas»):<br />Wo gibt es heute Probleme und Herausforderungen? An welchen Stellen im Flow besteht Potential für Verbesserung bzw. Innovation? | A    | |
| 1 <br/>(12) | Mittels Kreativitätsmethode (z.B. 6-3-5) mögliche Lösungsansätze generieren | B    | |
<mark> == Tag 4  </mark>
| 2 <br/>(14) | Visualisierung der Lösungsansätze auf Pen & Paper, Bewertung in der Gruppe und Priorisierung (= "Design Studio"), Ideenkatalog zusammenstellen | B    | Papier und Bleistift |
| 2 <br/>(16) | Soll-Zustand des Flow erstellen: Ziel, Aufgaben, Teilaufgaben (="User Story Map") | B    | |
<mark> == Tag 5  </mark>
| 2 <br/>(18) | Prototyp der Key Screens entwerfen (z.B. «Paper-Prototyp») anhand der formulierten User Story Map | B    | Figma oder Balsamiq |
| 2 <br/>(20) | Bildhafte Darstellung des Flows mittels den Key Screens (= "User Journey") | B    | Papier und Bleistift |
<mark> == Tag 6  </mark>
| 2 <br/>(22) | Erste Validierung der Key Screens (z.B. Hallway Testing, Expertreview) <br />Aufzeigen, dass bereits mit diesem Stand wichtige Learnings gewonnen werden können. Rückführung in Prototyp | B    | |
| 2 <br/>(24) | Ausarbeitung bzw. Umsetzung des Prototypen in einem clickable Low-Fi Prototyp | C, D   | Figma oder Bubble |
<mark> == Tag 7  </mark>
| 2 <br/>(26) | User Test vorbereiten:<br />Kritische Stellen im Prototyp identifizieren, dafür auch die Annahmen-Map zur Hilfe nehmen<br />Konkrete Fragestellungen und Aufgaben für den User Test ableiten | C, D   | |
| 2 <br/>(28) | User Test durchführen mit mind. 5 Personen (Nielsens 5 User Regel)<br /><br />Findings festhalten z.B. in einem Excel-Sheet o.ä. | C, D   | MS Teams, Zoom, Solid User Tests<br /><br />Excel oder PowerPoint |
<mark> == Tag 8  </mark>
| 2 <br/>(30) | Prototyp gemäss den Findings aus dem User Test anpassen  | C, D   | |
| 2 <br/>(32) | Sensibilisierung auf das Thema Accesbility<br />Überprüfen der Elemente im Prototyp auf Barrierefreiheit | E   | Microsoft’s Inclusive Design Toolkit /<br />Userway /<br />spezifische Plug-Ins für Figma |
<mark> == Tag 9  </mark>
| 2 <br/>(34) | Optimierung der Elemente und Aufzeigen der Anpassungen bzgl. Barrierefreiheit. | E   | |
| 2 <br/>(36)| Abgabe und/oder Präsentation und Bewertung der Lernprodukte (zusammen mit den Lernenden) | | |
<mark> == Tag 10  </mark>



<br>
<br>
<br>

<hr>

<br>
<br>
<br>
<br>

# Kompetenz-Matrix

| Kompetenzband: | HZ | Grundlagen      | Fortgeschritten | Erweitert      |
| -------------- | -- | --------------- | --------------- | -------------- |
| Band A<br />**Analysiert Benutzereigenschaften und Nutzungsumfeld bezüglich der mit dem System zu lösender Aufgabe und dokumentiert sie** |  **1**  |   |   |   |
| Anforderungsanalyse verstehen | 1.1 | A1G:<br/>Ich kann die Bestandteile einer Anforderungsanalyse nennen (z.B.  User, Aufgaben, Kontext und Technologie) | A1F:<br/>Ich kann die Ergebnisse einer Anforderungsanalyse erläutern (z.B. User, Aufgaben, Kontext und Technologie). | A1E:<br/>Ich kann eine bestehende Anforderungsanalyse hinterfragen und Verbesserungen vorschlagen. |
| Vorgehensmodell wählen | 1.2 | A2G:<br/>Ich kann ein geeignetes Vorgehensmodell zur Erhebung von Anforderung nennen und seine Phasen aufzählen (z.B. Design Thinking, User Centered Design). | A2F:<br/>Ich kann den Sinn und Zweck eines Vorgehensmodells erklären und seine Phasen erläutern (z.B. Design Thinking, User Centered Design). | A2E:<br/>Ich kann ein iteratives Vorgehensmodell begründet auswählen. |
| Nutzungskontext verstehen | 1.3 | A3G:<br/>Ich kann eine Methode zur Analyse von Nutzungsumfeld, Aufgaben und Benutzerverhalten erklären (z.B. Beobachtung, Interview, Loganalyse). | A3F:<br/>Ich kann eine Methode zur Analyse von Nutzungsumfeld, Aufgaben und Benutzerverhalten anwenden (z.B. Beobachtung, Interview, Loganalyse). | A3E:<br/>Ich kann die gewonnen Erkenntnisse aus der Anwendung einer Methode sinnvoll in Anforderungen überführen. |
| Benutzereigenschaften erfassen | 1.4 | A4G<br/>Ich kann den Zweck einer methodischen Erfassung von Benutzereigenschaften, Zielen und Bedürfnissen begründen. | A4F:<br/>Ich kann aus Benutzereigenschaften, Zielen und Bedürfnissen die wichtigsten Benutzerprofile ableiten und methodisch erfassen (z.B. Persona, Empathy Map). | A4E:<br/>Ich kann Benutzereigenschaften, Ziele und Bedürfnisse hinterfragen und Benutzerprofile dahingehend optimieren. |
| Nutzungsanforderungen spezifizieren | 1.5 | A5G:<br/>Ich kann eine Methode zur Dokumentation und Spezifikation von Nutzungsanforderungen erklären (z.B. Use Cases, User Stories). | A5F:<br/>Ich kann eine Methode zur Dokumentation und Spezifikation von Nutzungsanforderungen anwenden (z.B. Use Cases, User Stories). | A5E:<br/>Ich kann eine Methode zur Bewertung und Priorisierung von Nutzungsanforderungen anwenden (z.B. Backlog). |
|  |  |  |  |  |
| Band B<br />**Entwirft Varianten einer Benutzerschnittstelle (Maske und Abfolge) anhand vorgegebener Standards und Ergonomieanforderungen** |  **2**  |   |   |   |
| Grundsätze der Dialoggestaltung verstehen | 2.1 | B1G:<br />Ich kann das Konzept der Gebrauchstauglichkeit (Usability) erklären (Effektivität, Effizienz, Zufriedenstellung). | B1F:<br />Ich kann die Interaktionsprinzipien einer bestehenden Benutzerschnittstelle erklären (z.B. nach ISO 9241-110). | B1E:<br />Ich kann Interaktionsprinzipien für eine geforderte Benutzerschnittstelle festlegen. |
| Benutzerschnittstelle entwerfen | 2.2 | B2G:<br />Ich kann einzelne Dialoge (Screens, z.B. als Wireframe) skizzieren. | B2F:<br />Ich kann eine Abfolge (Flow) von Dialogen (Screens) als Papierprototyp präsentieren. | B2E:<br />Ich kann eine Abfolge (Flow) einer Benutzerschnittstelle als Klickprototypen entwerfen. |
| Interaktionsprinzipien anwenden | 2.3 | B3G:<br />Ich kann verschiedene Interaktionselemente einer bestehenden Benutzerschnittstelle identifzieren (z.B. Menu, Navigation, Orientierung, Ordnung). | B3F:<br />Ich kann für eine Benutzerschnittstelle passende Interaktionselemente nach entsprechenden Konventionen auswählen und sinnvoll anordnen (z.B. semantischer Aufbau, Darstellung, Benennung, Struktur) | B3E:<br />Ich kann eine bestehende Benutzerschnittstelle in Bezug auf die Interaktionselemente analysieren und optimieren. |
| Eingabeformate kennzeichnen | 2.4 | B4G:<br />Ich kann Regeln zur Kennzeichnung von Pflichtfeldern nennen (z.B. Sternchen). | B4F:<br />Ich kann Regeln zur Kennzeichnung von erwarteten Eingabeformaten nennen (z.B. Datum, E-Mail, Telefon). | B4E:<br />Ich kann Pflichtfelder und erwartete Eingabeformate einer Benutzerschnittstelle selbsterklärend kennzeichnen. |
| Hilfe und Feedback integrieren | 2.5 | B5G:<br />Ich kann geeignete Hilfefunktionen für eine Benutzerschnittstelle identifizieren und vorschlagen. | B5F:<br />Ich kann geeignete Feedbackinformationen für eine Benutzerschnittstelle identifizieren und vorschlagen. | B5E:<br />Ich kann sämtliche Hilfefunktionen und Feedbackinformationen einer Benutzerschnittstelle abbilden. |
|  |  |  |  |  |
| Band C<br />**Implementiert eine Benutzerschnittstelle gemäss Entwurf und überprüft problematische Teile auf Machbarkeit** |  **3**  |   |   |   |
| Elemente einer Benutzerschnittstelle kennen und anwenden | 3.1 | C1G:<br />Ich kenne einfache Controls und Widgets und deren Eigenschaften für Benutzerschnittstellen und kann diese benennen und erklären. | C1F:<br />Ich kann verschiedene Controls und Widgets für ein Interaktionsziel auswählen und sinnvoll platzieren.  | C1E:<br />Ich kann komplexere und zusammengesetzte Interaktionselemente entwerfen und in eine logische Abfolge bringen. |
| Benutzerschnittstelle erstellen und testen | 3.2 | C2G:<br />Ich setze die entworfenen Skizzen in einen klickbaren Prototyp um. | C2F:<br />Ich kann ein Vorhaben mit einem klickbaren Prototyp testen und mögliche Probleme identifizieren | C2E:<br />Ich kann Probleme analysieren, Verbesserungsvorschläge anbringen und den Prototypen überarbeiten |
| Benutzerfreundlichkeit verbessern | 3.3 | C3G:<br/>Bezüglich der Benutzerfreundlichkeit erkenne ich schwierig umzusetzende Elemente | C3F:<br/>Ich kann Vorschläge zur Verbesserung Benutzerferundlichkeit unterbreiten | C3E:<br/>Ich kann aus einem Element bezüglich Benutzbarkeit eine bessere, einfachere und intuitive Lösung umsetzen (erarbeiten) |
|  |  |  |  |  |
| Band D<br />**Überprüft eine Benutzerschnittstelle auf Ergonomie** |  **4**  |   |   |   |
| Usability testen | 4.1 | D1G:<br />Ich kann einen Walktrough einer Benutzerschnittstellen-Abfolge mit jemandem durchführen | D1F:<br />Ich kann einen Usability-Test für eine Abfolge von Benutzerschnittstellen bezüglich Benutzerfreundlichkeit durchführen | D1E:<br />Ich kann einen Usability-Test für eine Abfolge von Benutzerschnittstellen bezüglich Benutzerfreundlichkeit und auch Business-Effizienz planen und durchführen |
| Usablility quantifizieren und verbessern | 4.2 | D2G:<br />Ich kann einen standardisierten Fragebogen (SUS, HEART) erklären.  | D2F:<br />Ich kann anhand des Fragebogens (SUS, HEART) eine Nachbefragung durchführen und auswerten. | D1E:<br />Ich kann Ergebnisse (Metrik) analysieren und Verbesserungsvorschläge anbringen. |
|  |  |  |  |  |
| Band E<br />**Implementiert eine Benutzerschnittstelle barrierefrei und überprüft sie** |  **5**  |   |   |   |
| Barrierefreiheit umsetzen | 5.1 | E1G:<br />Ich kann die Anforderungen an eine barrierefreie Benutzerschnittstelle nennen und kann erklären worauf es beim Entwurf einer Anwendung ankommt. | E1F:<br />Ich kann Elemente einer Benutzerschnittstelle barrierefrei umsetzen (Labels, Tab-Navigation, Kontrast, Alternativtext usw.). | E1E:<br />Ich kann geeignete Elemente für eine barrierefreie Benutzerschnittstelle vorschlagen. |
| Barrierefreiheit prüfen |5.2| E2G:<br />Ich kann die Möglichkeiten zur Überprüfung der Barrierefreiheit erläutern (z.B. Checkliste). | E2F:<br />Ich kann eine Benutzerschnittstelle gemäss Checkliste auf Barrierefreiheit prüfen. | E2E:<br />Ich kann die Barrierefreiheit einer Benutzerschnittstelle nachweisen. |

